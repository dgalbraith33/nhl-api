from nhlapi import Team
from pytest import fixture, raises


@fixture
def team_attrs():
    return ["id", "name", "link", "venue", "abbreviation", "teamName", "locationName",
            "firstYearOfPlay", "division", "conference", "franchise", "shortName",
            "officialSiteUrl", "franchiseId", "active"]


def test_team_full(team_attrs):
    bruins_id = 6
    team = Team(bruins_id)
    full = team.full()

    assert isinstance(full, dict)
    assert full["id"] == bruins_id
    assert set(team_attrs).issubset(full.keys())


def test_team_nonexistent():
    bad_id = 9999
    team = Team(bad_id)
    with raises(KeyError) as error:
        team.full()
    assert "Team with id" in str(error.value)


def test_team_property_id():
    bruins_id = 6
    team = Team(bruins_id)

    assert team.id == bruins_id


def test_team_property_name():
    bruins_id = 6
    team = Team(bruins_id)

    assert team.name == "Boston Bruins"


def test_team_property_abbr():
    bruins_id = 6
    team = Team(bruins_id)

    assert team.abbr == "BOS"


def test_list_teams(team_attrs):
    teams = Team.all()

    assert len(teams) == 31
    for team in teams:
        assert set(team_attrs).issubset(team.keys())
