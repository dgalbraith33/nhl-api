import requests


class Team(object):

    def __init__(self, api_id):
        self.api_id = api_id
        self.api_resp = None

    def __load_if_necessary(self):
        if self.api_resp is None:
            team_url = "https://statsapi.web.nhl.com/api/v1/teams/{}"
            response = requests.get(team_url.format(self.api_id)).json()
            if "teams" not in response:
                raise KeyError("Team with id {} not found".format(self.api_id))
            self.api_resp = response["teams"][0]

    def full(self):
        self.__load_if_necessary()
        return self.api_resp

    @property
    def id(self):
        return self.api_id

    @property
    def name(self):
        self.__load_if_necessary()
        return self.api_resp["name"]

    @property
    def abbr(self):
        self.__load_if_necessary()
        return self.api_resp["abbreviation"]

    @staticmethod
    def all():
        list_url = "https://statsapi.web.nhl.com/api/v1/teams"
        response = requests.get(list_url).json()
        return response["teams"]
